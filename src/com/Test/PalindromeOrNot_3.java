package com.Test;

public class PalindromeOrNot_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		 int a = 121;
	     int b = 125;//enter input values
	     checkPalindrome(a);//check the values
	     checkPalindrome(b);
	 }
	 public static void checkPalindrome(int num) {//here by creating method for integer
	     int rem, rev = 0;
	     for (int i = num; i > 0; i /= 10) {//by temp variable i
	         rem = i % 10;
	         rev = rev * 10 + rem;
	     }
	     if (rev == num) {
	         System.out.println(num + " is a palindrome.");
	     } else {
	         System.out.println(num + " is not a palindrome.");
	     }
	 }
	}