package com.Test;

public class ReverseArray_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] a= {11,19,15,10};
		System.out.println("Original Array : ");
		
		for(int i=0;i<a.length;i++) {
			System.out.println(a[i] +" ");
		}
		System.out.println("Arrays in reverse order : ");
		for(int i=a.length-1;i>=0;i--) {
			System.out.println(a[i] +" ");
		}
	}

}
